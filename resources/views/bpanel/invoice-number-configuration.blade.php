<livewire:form::input-number
    name="next_invoice_number"
    :labelText="__('bpanel4-invoices::shop-configuration.next-invoice-number')"
    :required=true
    :value="old('next_invoice_number') ?? $currentNextInvoiceNumber ?? 1"
    min=0
    step=1
    fieldWidth=7
    labelWidth=3
/>