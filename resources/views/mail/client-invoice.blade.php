@extends('bpanel4-public.mail.base-mail')
<?php /**
 * @var \Bittacora\Bpanel4\Orders\Models\Order\OrderClientDetail $client
 * @var \Bittacora\Bpanel4\Orders\Models\Order\OrderProduct[] $products
 * @var \Bittacora\Bpanel4\Orders\Models\Order\Order $order
 */ ?>
@section('preheader')@endsection
@section('content')
    <p>{{ $client->getName() }}, hemos generado la factura para su pedido, la podrá encontrar como adjunto en este correo, y también desde el apartado "Mi cuenta" de nuestra web.</p>
@endsection
