<?php
/**
 * @var \Bittacora\Bpanel4\Invoices\Dtos\InvoiceDto $dto
 */
?>
<html>
<head>
    <style>
        @page {
            margin-top: 1cm;
            margin-bottom: 0;
        }

        body {
            font-size: 12pt;
            font-family: Helvetica;
            line-height: 1.4em;
        }

        div.container {
            margin: 0 auto;
        }

        table {
            border-collapse: collapse;
            width: 100%;
        }

        table td,
        table th {
            padding: 3px 0;
        }

        h2 {
            margin-bottom: 7px;
        }

        .header-row td {
            width: 50%;
        }

        .text-center {
            text-align: center !important;
        }

        .text-left {
            text-align: left !important;
        }

        .text-right {
            text-align: right !important;
        }

        .logo-container img {
            width: 100%;
            max-width: 195px;
            margin-top: -.5cm;
        }

        .addresses-table {
            font-size: 9pt;
            line-height: 1.2em;
            width: 100%;
        }

        .addresses-table td,
        .addresses-table th {
            text-align: left;
            vertical-align: top;
        }


        .addresses-table table th {
            width: 65pt;
        }

        .border-top {
            border-top: 1px solid;
        }

        .border-bottom {
            border-bottom: 1px solid;
        }

        .article-table {
            font-size: 9pt;
            margin-top: .85cm;
        }

        .article-table thead tr {
            background: #000;
            color: white;
        }

        .article-table thead tr td,
        .article-table thead tr th {
            padding: .1cm .1cm;
            text-align: left;
        }

        .shop-address {
            font-size: 12px;
            padding-left: 60px;
            line-height: 1.4em;
        }

        .invoice-title {
            padding-top: 45px;
            font-weight: bold;
            font-size: 1.3rem;
            padding-bottom: .4cm;
        }

        .invoice-details th {
            white-space: nowrap;
            font-weight: normal;
        }

        .invoice-details td {
            padding-left: .6cm;
            text-align: left;
        }

        footer {
            border-top: 1px solid #000;
            font-size: 12px;
            padding-top: .125cm;
            position: fixed;
            left: 50%;
            transform: translateX(-50%);
            right: 0;
            height: 50px;
            bottom: 0;
            width: 17cm;
        }


        .addresses-table > tbody > tr > td.shipping-address {
            width: 10cm;
        }

        .addresses-table > tbody > tr > td.invoice-details {
            width: 6.7cm;
        }

        .addresses-table.has-2-addresses > tbody > tr > td.billing-address {
            width: 5cm;
        }

        .addresses-table.has-2-addresses > tbody > tr > td.shipping-address {
            width: 5cm;
        }

        .addresses-table.has-2-addresses > tbody > tr > td.invoice-details {
            width: 6.7cm;
        }

        .shipping-address th,
        .billing-address th {
            display: none;
        }

        .shipping-address td,
        .billing-address td {
            padding: .05cm 0;
        }

        .total-price-row .bordered {
            border-top: 1px solid #000 !important;
            border-bottom: 1px solid #000 !important;
        }

        .padding-right {
            padding-right: .5cm
        }

        .product-row.last {
            border-bottom: 1px solid;
        }

        .product-column {
            width: 9cm;
        }

        .quantity-column {
            text-align: left !important;
            padding-left: 1.1cm !important;
            width: 3.2cm;
        }

        .price-column {
            text-align: left;
        }

        td.price-column {
            padding-left: .1cm;
        }

        .coupon-row td,
        .subtotal-row td,
        .shipping-row td,
        .total-price-row td,
        .tax-breakdown-row td {
            text-align: left !important;
            padding-left: 1.1cm;
        }

        .subtotal-row td:nth-child(3),
        .shipping-row td:nth-child(3),
        .total-price-row td:nth-child(3),
        .tax-breakdown-row td:nth-child(3) {
            padding-left: .1cm;
        }
        span.product-attribute-in-name {
            display: block;
            font-size: 0.75em;
            line-height: 1em;
        }

        .order-notes {
            font-size: 10pt;
        }
    </style>
</head>
<body>
    <div class="container">
        <table>
            <tr class="header-row">
                <td class="logo-container text-left">
                    @if(!empty($dto->logoPath))
                        <img src="{{ $dto->logoPath }}" class="logo">
                    @endif
                </td>
                <td class="shop-address">
                    <strong>{{ $dto->shopName }}</strong><br/>
                    {{ $dto->shopCif }}<br/>
                    {!! nl2br($dto->shopAddress) !!}
                </td>
            </tr>
        </table>

        <table>
            <tr>
                <td class="invoice-title">FACTURA</td>
            </tr>
        </table>

        <table class="addresses-table @if($dto->billingAddressIsDifferentFromShippingAddress()) has-2-addresses @endif">
            <tbody>
                <tr>
                    @if($dto->billingAddressIsDifferentFromShippingAddress())
                        <td class="billing-address">
                            {{--<h2>Dirección de facturación</h2>--}}
                            <table>
                                <tr>
                                    <th>Nombre:</th>
                                    <td>{{ $dto->billingName }} {{ $dto->billingSurname }}</td>
                                </tr>
                                <tr>
                                    <th>DNI/NIF/NIE:</th>
                                    <td>{{ $dto->billingNif }}</td>
                                </tr>
                                <tr>
                                    <th>Dirección:</th>
                                    <td>{{$dto->billingAddress}}</td>
                                </tr>
                                @if($dto->billingCountry !== 'España')
                                    <tr>
                                        <th>País:</th>
                                        <td>{{ $dto->billingCountry }}</td>
                                    </tr>
                                @endif
                                <tr>
                                    <th>Localidad:</th>
                                    <td>{{ $dto->billingPostalCode }} {{ $dto->billingLocation }}</td>
                                </tr>
                                <tr>
                                    <th>Provincia:</th>
                                    <td>{{ $dto->billingState }}
                                    </td>
                                </tr>
                                @if($dto->billingPhone !== $dto->clientPhone)
                                    <tr>
                                        <th>Teléfono</th>
                                        <td>{{ $dto->billingPhone !== '' ? $dto->billingPhone : $dto->clientPhone }}</td>
                                    </tr>
                                @endif
                                <tr>
                                    <th>Email</th>
                                    <td>{{ $dto->clientEmail }}</td>
                                </tr>
                            </table>
                        </td>
                    @endif
                    <td class="shipping-address">
                        @if($dto->billingAddressIsDifferentFromShippingAddress())
                            <strong>Enviar a:</strong>
                        @endif
                        <table>
                            <tr>
                                <th>Nombre:</th>
                                <td>{{ $dto->shippingName }} {{ $dto->shippingSurname }}</td>
                            </tr>
                            @if(!$dto->billingAddressIsDifferentFromShippingAddress())
                                <tr>
                                    <th>DNI/NIF/CIF:</th>
                                    <td>{{ $dto->billingNif }}</td>
                                </tr>
                            @endif
                            <tr>
                                <th>Dirección</th>
                                <td>{{$dto->shippingAddress}}</td>
                            </tr>
                            @if($dto->shippingCountry !== 'España')
                                <tr>
                                    <th>País</th>
                                    <td>{{ $dto->shippingCountry }}</td>
                                </tr>
                            @endif
                            <tr>
                                <th>Localidad</th>
                                <td>{{ $dto->shippingPostalCode }} {{ $dto->shippingLocation }}</td>
                            </tr>
                            @if($dto->shippingLocation !== 'Badajoz')
                                <tr>
                                    <th>Provincia</th>
                                    <td>{{ $dto->shippingState }}</td>
                                </tr>
                            @endif
                            @if($dto->shippingPhone !== $dto->clientPhone)
                                <tr>
                                    <th>Teléfono</th>
                                    <td>{{ $dto->shippingPhone != ''? $dto->shippingPhone : $dto->clientPhone }}</td>
                                </tr>
                            @endif
                            <tr>
                                <th>Email</th>
                                <td>{{ $dto->clientEmail }}</td>
                            </tr>
                        </table>
                    </td>
                    <td class="invoice-details">
                        <table>
                            <tr>
                                <th>Número de factura:</th>
                                <td> {{ str_pad($dto->invoiceNumber, 6, '0', STR_PAD_LEFT) }}</td>
                            </tr>
                            <tr>
                                <th>Fecha de factura:</th>
                                <td> {{ $dto->invoiceDate }}</td>
                            </tr>
                            <tr>
                                <th>Número de pedido:</th>
                                <td> {{ $dto->orderNumber }}</td>
                            </tr>
                            <tr>
                                <th>Fecha de pedido:</th>
                                <td> {{ $dto->orderDate }}</td>
                            </tr>
                            <tr>
                                <th>Método de pago:</th>
                                <td> {{ $dto->paymentMethod }}</td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
        <table class="article-table">
            <thead>
                <tr>
                    <th class="border-bottom product-column">Producto</th>
                    <th class="border-bottom unit-price-column">Precio unitario</th>
                    <th class="border-bottom quantity-column">Cantidad</th>
                    <th class="border-bottom price-column">Precio</th>
                </tr>
            </thead>
            <tbody>
                @foreach($dto->products as $product)
                    <tr class="product-row @if($loop->last) last @endif">
                        <td class="product-column">
                            {!! $product->name !!}
                            <div>
                                @if(is_array($product->meta))
                                    @foreach($product->meta as $field)
                                        <strong>{{ $field['name'] }}:</strong> {{ $field['value'] }}<br>
                                    @endforeach
                                @endif
                            </div>
                        </td>
                        <td class="unit-price-column">{{ number_format($product->unitPrice, 2, ',', '.') }} €</td>
                        <td class="quantity-column">{{ $product->quantity }}</td>
                        <td class="price-column">{{ number_format($product->total, 2, ',', '.') }} €</td>
                    </tr>
                @endforeach

                @foreach($dto->giftedProducts as $giftedProduct)
                    <tr class="product-row @if($loop->last) last @endif">
                        <td class="product-column">
                            {!! $giftedProduct->name !!}
                            <div>
                                @if(is_array($giftedProduct->meta))
                                    @foreach($giftedProduct->meta as $field)
                                        <strong>{{ $field->name }}:</strong> {{ $field->value }}<br>
                                    @endforeach
                                @endif
                            </div>
                        </td>
                        <td class="quantity-column">{{ $giftedProduct->quantity }}</td>
                        <td class="price-column">{{ __('¡Gratis!') }}</td>
                    </tr>
                @endforeach
                @foreach($dto->coupons as $coupon)
                    <tr class="coupon-row">
                        <td></td>
                        <td>Cupón descuento '{{ $coupon['code'] }}'</td>
                        <td class=" ">{{ $coupon['discount'] }}</td>
                    </tr>
                @endforeach
                <tr class="subtotal-row">
                    <td></td>
                    <td class="border-top text-right">Subtotal</td>
                    <td class=" border-top ">{{ number_format($dto->subtotalWithoutTaxes, 2, ',', '.') }} €</td>
                </tr>
                <tr class="shipping-row">
                    <td></td>
                    <td class="">Envío</td>
                    <td class="">{{ number_format($dto->shippingCosts, 2, ',', '.') }} €</td>
                </tr>
                @if (!empty($dto->taxBreakdown))
                    @foreach($dto->taxBreakdown as $breakdown)
                        @if($breakdown->getTotalWithoutTaxes() > 0)
                            <tr class="tax-breakdown-row">
                                <td></td>
                                <td>{{ $breakdown->getName() }}% IVA
                                                               de {{ number_format($breakdown->getTotalWithoutTaxes(), 2, ',', '.') }}
                                                               €
                                </td>
                                <td>{{ number_format($breakdown->getAppliedTaxes(), 2, ',', '.') }} €</td>
                            </tr>
                        @endif
                    @endforeach
                @endif
                <tr class="total-price-row">
                    <td></td>
                    <td class=" bordered"><strong>Total</strong></td>
                    <td class=" bordered "><strong>{{ number_format($dto->orderTotal, 2, ',', '.') }} €</strong></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
            </tbody>
        </table>

        @if(null !== $dto->orderNotes)
            <div class="order-notes">
                <strong>Notas del pedido:</strong> {{ $dto->orderNotes }}
            </div>
        @endif

    </div>
    <footer class="text-center">
    </footer>
</body>
</html>