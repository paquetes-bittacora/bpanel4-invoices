<?php

declare(strict_types=1);

return [
    'next-invoice-number' => 'Nº de la siguiente factura',
];