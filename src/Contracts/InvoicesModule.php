<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Invoices\Contracts;

use Bittacora\Bpanel4\Invoices\Models\Invoice;
use DateTime;

interface InvoicesModule
{
    /**
     * Comprueba si un número de factura ya está en uso. Si se pasa $orderNumber ignorará el pedido con ese número de
     * pedido.
     */
    public function invoiceNumberIsTaken(int $invoiceNumber, int $orderNumber = -1): bool;

    public function getInvoiceNumberForOrderId(int $getId): ?int;

    public function getInvoiceForOrderId(int $getId): ?Invoice;

    public function updateInvoiceDate(Invoice $invoice, DateTime $dateTime): void;
}