<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Invoices\Contracts;

use Bittacora\Bpanel4\Orders\Models\Order\Order;

interface InvoiceDeleter
{
    public function deleteInvoiceForOrder(Order $order): void;
}