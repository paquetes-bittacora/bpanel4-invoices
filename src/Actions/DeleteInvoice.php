<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Invoices\Actions;

use Bittacora\Bpanel4\Invoices\Contracts\InvoiceDeleter;
use Bittacora\Bpanel4\Invoices\Models\Invoice;
use Bittacora\Bpanel4\Orders\Models\Order\Order;

final class DeleteInvoice implements InvoiceDeleter
{
    public function deleteInvoiceForOrder(Order $order): void
    {
        Invoice::where('order_id', $order->getId())->whereNotNull('invoice_path')->delete();
    }
}