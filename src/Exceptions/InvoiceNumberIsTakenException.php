<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Invoices\Exceptions;

use Exception;

final class InvoiceNumberIsTakenException extends Exception
{
    /** @var string $message */
    protected $message = 'El número de factura introducido ya está en uso.';
}