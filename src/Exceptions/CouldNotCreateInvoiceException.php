<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Invoices\Exceptions;

use Exception;

final class CouldNotCreateInvoiceException extends Exception
{
    /** @var string $message */
    protected $message = 'No se pudo generar la factura';
}
