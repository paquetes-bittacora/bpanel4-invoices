<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Invoices\Http\Controllers;

use App\Http\Controllers\Controller;
use Bittacora\Bpanel4\Invoices\Exceptions\CouldNotCreateInvoiceException;
use Bittacora\Bpanel4\Invoices\Models\Invoice;
use Doctrine\DBAL\Driver\Exception;
use Illuminate\Http\Request;
use Response;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Throwable;

final class BpanelInvoicesPublicController extends Controller
{
    /**
     * @throws Throwable
     * @throws CouldNotCreateInvoiceException
     * @throws Exception
     */
    public function downloadInvoice(Request $request, Invoice $invoice): BinaryFileResponse
    {
        if (!$request->hasValidSignature()) {
            throw new HttpException(403);
        }

        return Response::download(storage_path('app/public/' . $invoice->getInvoicePath()));
    }
}
