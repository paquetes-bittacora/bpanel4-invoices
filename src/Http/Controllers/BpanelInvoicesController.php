<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Invoices\Http\Controllers;

use App\Http\Controllers\Controller;
use Bittacora\Bpanel4\Invoices\Exceptions\CouldNotCreateInvoiceException;
use Bittacora\Bpanel4\Invoices\Models\Invoice;
use Bittacora\Bpanel4\Invoices\Services\OrderInvoiceGenerator;
use Bittacora\Bpanel4\Orders\Models\Order\Order;
use Doctrine\DBAL\Driver\Exception;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Throwable;

final class BpanelInvoicesController extends Controller
{
    /**
     * @throws Throwable
     * @throws CouldNotCreateInvoiceException
     * @throws Exception
     */
    public function getInvoice(Request $request, Order $order, OrderInvoiceGenerator $invoiceGenerator): void
    {
        if (!$request->hasValidSignature()) {
            throw new HttpException(403);
        }

        $invoiceGenerator->downloadInvoice($order);
    }
}
