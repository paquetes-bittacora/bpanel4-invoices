<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Invoices\Services;

use Bittacora\Bpanel4\Invoices\Contracts\InvoicesModule;
use Bittacora\Bpanel4\Invoices\Models\Invoice;
use DateTime;

final class InvoiceModule implements InvoicesModule
{
    public function invoiceNumberIsTaken(int $invoiceNumber, int $orderNumber = -1): bool
    {
        return Invoice::where('invoice_number', $invoiceNumber)->whereNot('order_id', $orderNumber)->exists();
    }

    public function getInvoiceNumberForOrderId(int $getId): ?int
    {
        return Invoice::where('order_id', $getId)->first()?->getInvoiceNumber();
    }

    public function getInvoiceForOrderId(int $getId): ?Invoice
    {
        return Invoice::where('order_id', $getId)->first();
    }


    public function updateInvoiceDate(Invoice $invoice, DateTime $dateTime): void
    {
        $invoice->created_at = $dateTime;
        $invoice->save();
    }
}