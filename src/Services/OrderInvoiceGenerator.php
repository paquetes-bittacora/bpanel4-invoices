<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Invoices\Services;

use Bittacora\Bpanel4\Invoices\Events\InvoiceHasBeenGenerated;
use Bittacora\Bpanel4\Invoices\Exceptions\CouldNotCreateInvoiceException;
use Bittacora\Bpanel4\Invoices\Models\Invoice;
use Bittacora\Bpanel4\Orders\Models\Order\Order;
use Doctrine\DBAL\Driver\Exception;
use Illuminate\Events\Dispatcher;
use Illuminate\Filesystem\FilesystemManager;
use Throwable;

final class OrderInvoiceGenerator
{
    public function __construct(
        private readonly InvoicePdfBuilder $builder,
        private readonly OrderToInvoiceDtoConverter $converter,
        private readonly FilesystemManager $storage,
        private readonly Dispatcher $dispatcher,
    ) {
    }

    /**
     * @throws CouldNotCreateInvoiceException
     * @throws Throwable
     * @throws Exception
     */
    public function downloadInvoice(Order $order): void
    {
        $file = $this->generateInvoice($order);

        header('Content-type: application/pdf');
        header('Content-Disposition: attachment; filename="' . basename($file) . '"');
        header('Content-Transfer-Encoding: binary');
        header('Content-Length: ' . filesize($file));
        header('Accept-Ranges: bytes');


        echo file_get_contents($file);
    }

    /**
     * @throws CouldNotCreateInvoiceException
     * @throws Throwable
     * @throws Exception
     */
    public function generateInvoice(Order $order, bool $forceEventDispatch = false): string
    {
        $invoiceExisted = Invoice::where('order_id', $order->getId())->whereNotNull('invoice_path')->exists();
        $file = $this->getFilePath($order);

        if (!$invoiceExisted || $forceEventDispatch) {
            $this->dispatcher->dispatch(new InvoiceHasBeenGenerated($order, $file));
        }

        return $file;
    }

    /**
     * @param Order $order
     * @return string
     * @throws CouldNotCreateInvoiceException
     * @throws Exception
     * @throws Throwable
     */
    public function getFilePath(Order $order): string
    {
        return $this->storage->disk('public')->path($this->builder->generate($this->converter->convert($order)));
    }

}
