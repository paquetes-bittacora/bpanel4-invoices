<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Invoices\Services;

use Bittacora\Bpanel4\Invoices\Dtos\InvoiceDto;
use Bittacora\Bpanel4\Invoices\Exceptions\CouldNotCreateInvoiceException;
use Bittacora\Bpanel4\Invoices\Models\Invoice;
use Dompdf\Dompdf;
use Exception;
use Illuminate\Filesystem\FilesystemManager;
use Illuminate\Foundation\Application;

/**
 * Clase que genera una factura en PDF a partir de un pedido.
 */
final class InvoicePdfBuilder
{
    private InvoiceDto $dto;

    public function __construct(
        private readonly FilesystemManager $storage,
        private readonly InvoiceHtmlRenderer $invoiceHtmlRenderer,
        private readonly Application $application,
    ) {
    }

    /**
     * @return string Ruta al archivo generado
     * @throws CouldNotCreateInvoiceException
     * @throws Exception
     */
    public function generate(InvoiceDto $invoiceDto): string
    {
        $invoice = Invoice::where('order_id', $invoiceDto->orderNumber)->whereNotNull('invoice_path')->first();
        if (null !== $invoice) {
            return $invoice->getInvoicePath();
        }

        $this->dto = $invoiceDto;
        $dompdf = new Dompdf();
        $dompdf->getOptions()->setChroot($this->application->storagePath());
        $dompdf->loadHtml($this->invoiceHtmlRenderer->render($invoiceDto));
        $dompdf->render();
        $output = $dompdf->output();
        $path = 'invoices/' . $this->getSubfolderName() . '/' .$invoiceDto->invoiceNumber . '_' .
            bin2hex(random_bytes(3)) . '.pdf';

        if (!$this->storage->disk('public')->put($path, $output)) {
            throw new CouldNotCreateInvoiceException();
        }

        $this->saveFilePath($path);

        return $path;
    }

    private function saveFilePath(string $path): void
    {
        $invoice = Invoice::where('prefixed_invoice_number', $this->dto->invoiceNumber)->firstOrFail();
        $invoice->setInvoicePath($path);
        $invoice->save();
    }

    private function getSubfolderName(): string
    {
        return str_pad(
            (string) floor((int) $this->dto->invoiceNumber / 100),
            6,
            '0',
            STR_PAD_LEFT
        );
    }
}
