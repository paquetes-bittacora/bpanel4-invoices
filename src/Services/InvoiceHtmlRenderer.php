<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Invoices\Services;

use Bittacora\Bpanel4\Invoices\Dtos\InvoiceDto;
use Illuminate\Contracts\Config\Repository;
use Illuminate\Contracts\View\Factory;

final class InvoiceHtmlRenderer
{
    public function __construct(
        private readonly Repository $config,
        private readonly Factory $view,
    ) {
    }

    public function render(InvoiceDto $dto): string
    {
        $view = $this->config->get('bpanel4-invoices.invoice_template');
        return $this->view->make($view, ['dto' => $dto])->render();
    }
}
