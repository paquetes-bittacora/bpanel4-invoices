<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Invoices\Services;

use Bittacora\Bpanel4\Invoices\Dtos\InvoiceProductDto;
use Bittacora\Bpanel4\Orders\Models\Order\OrderProduct;
use function json_decode;

final class OrderProductToInvoiceProductDtoConverter
{
    public function convert(OrderProduct $product): InvoiceProductDto
    {
        return new InvoiceProductDto(
            name: $product->getName(),
            quantity: (string) $product->getQuantity(),
            unitPrice: $product->getPaidUnitPrice()->toFloat(),
            taxRate: (string) $product->getTaxRate(),
            unitPriceWithTaxes: $product->getPaidUnitPrice()->toFloat() + $product->getTaxAmount()->toFloat(),
            total: $product->getTotal()->toFloat(),
            reference: $product->getReference(),
            meta: is_null($product->meta) ? [] : json_decode($product->meta, true),
        );
    }
}
