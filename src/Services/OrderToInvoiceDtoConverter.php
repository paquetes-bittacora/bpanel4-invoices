<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Invoices\Services;

use Bittacora\Bpanel4\Invoices\Dtos\InvoiceDto;
use Bittacora\Bpanel4\Invoices\Dtos\InvoiceProductDto;
use Bittacora\Bpanel4\Invoices\Models\Invoice;
use Bittacora\Bpanel4\Orders\Models\Order\Order;
use Bittacora\Bpanel4\Orders\Models\Order\OrderProduct;
use Bittacora\Bpanel4\Prices\Types\Price;
use Bittacora\Bpanel4\ShopConfiguration\Models\ShopConfiguration;
use Bittacora\LivewireCountryStateSelector\Models\Country;
use Bittacora\LivewireCountryStateSelector\Models\State;
use Exception;
use Illuminate\Database\Connection;
use Illuminate\Database\Eloquent\Collection;
use Throwable;

/**
 * Convierte un Order a un InvoiceDto
 */
final class OrderToInvoiceDtoConverter
{
    private Invoice $invoice;
    private readonly ShopConfiguration $shopConfiguration;

    public function __construct(
        private readonly Connection $db,
        private readonly OrderProductToInvoiceProductDtoConverter $orderProductConverter,
        private readonly TaxBreakdownCalculator $taxBreakdownCalculator,
    ) {
        $this->shopConfiguration = $this->getShopConfiguration();
    }

    /**
     * @throws \Doctrine\DBAL\Driver\Exception
     * @throws Exception|Throwable
     */
    public function convert(Order $order): InvoiceDto
    {
        $this->db->beginTransaction();

        try {
            $this->createInvoice($order->getId());
            $client = $order->getClient();
            $shippingDetails = $order->getShippingDetails();
            $billingDetails = $order->getBillingDetails();

            $dto = new InvoiceDto(
                invoiceNumber: $this->invoice->getPrefixedInvoiceNumber(),
                invoiceDate: $this->getInvoiceDate(),
                shopName: $this->shopConfiguration->getName(),
                shopCif: $this->shopConfiguration->getCif(),
                shopAddress: $this->shopConfiguration->getAddress(),
                clientName: $client->getName() . ' ' . $client->getSurname(),
                clientNif: $client->getNif(),
                shippingName: $shippingDetails->getPersonName() !== '' ?
                    $shippingDetails->getPersonName() :
                    $client->getName(),
                shippingSurname: $shippingDetails->getPersonSurname() !== '' ?
                    $shippingDetails->getPersonSurname() :
                    $client->getSurname(),
                shippingAddress: $shippingDetails->getAddress(),
                shippingLocation: $shippingDetails->getLocation(),
                shippingPostalCode: $shippingDetails->getPostalCode(),
                shippingCountry: $this->getCountryName($shippingDetails->getCountryId()),
                shippingState: $this->getStateName($shippingDetails->getStateId()),
                billingName: $billingDetails->getPersonName() !== '' ?
                    $billingDetails->getPersonName() :
                    $client->getName(),
                billingSurname: $billingDetails->getPersonSurname() !== '' ?
                    $billingDetails->getPersonSurname() :
                    $client->getSurname(),
                billingAddress: $billingDetails->getAddress(),
                billingLocation: $billingDetails->getLocation(),
                billingPostalCode: $billingDetails->getPostalCode(),
                billingCountry: $this->getCountryName($billingDetails->getCountryId()),
                billingState: $this->getStateName($billingDetails->getStateId()),
                orderDate: $order->getDate()->format('d/m/Y'),
                orderNumber: $this->getOrderNumber($order),
                products: $this->formatProducts($order->getProducts()),
                subtotalWithoutTaxes: Price::fromInt($order->getSubtotalWithoutTaxes())->toFloat(),
                shippingCosts: Price::fromInt($order->getShippingCosts())->toFloat(),
                shippingCostsTaxes: Price::fromInt($order->getShippingCostsTaxes())->toFloat(),
                orderTotal: Price::fromInt($order->getOrderTotal())->toFloat(),
                logoPath: $this->shopConfiguration->getInvoiceLogo()?->getPath() ?? '',
                taxBreakdown: $this->taxBreakdownCalculator->calculateBreakdown(
                    $order->getProducts(),
                    Price::fromInt($order->getShippingCosts()),
                    $order->getDiscountByUnit(),
                ),
                vatNumber: $order->getVatNumber(),
                paymentMethod: $order->payment_method_name,
                clientPhone: $order->getClient()->getPhone(),
                clientEmail: $order->getClient()->getEmail(),
                coupons: $order->getCoupons()->toArray(),
                orderNotes: $order->getOrderNotes(),
                shippingPhone: $order->getShippingDetails()->getPersonPhone(),
                billingPhone: $order->getBillingDetails()->getPersonPhone(),
                giftedProducts: $this->formatProducts($order->getGiftedProducts()),
                shippingNif: $order->getShippingDetails()->getPersonNif(),
                billingNif: $order->getBillingDetails()->getPersonNif(),
            );

            $this->db->commit();

            return $dto;
        } catch (Exception $exception) {
            report($exception);
            $this->db->rollBack();
            throw $exception;
        }
    }

    private function getCountryName(int $id): string
    {
        return Country::whereId($id)->firstOrFail()->getName();
    }

    private function getStateName(int $id): string
    {
        return State::whereId($id)->firstOrFail()->getName();
    }

    private function createInvoice(int $orderId): void
    {
        $invoice = Invoice::firstOrCreate(['order_id' => $orderId]);

        $invoice->setOrderId($orderId);
        $invoice->save();
        $invoice->refresh();

        $this->invoice = $invoice;
    }

    private function getInvoiceDate(): string
    {
        return $this->invoice->getInvoiceDate()->format('d/m/Y');
    }

    private function getShopConfiguration(): ShopConfiguration
    {
        return ShopConfiguration::whereId(1)->firstOrFail();
    }

    /**
     * @param Collection<int, OrderProduct> $products
     * @return array<int, InvoiceProductDto>
     */
    private function formatProducts(Collection $products): array
    {
        $output = [];

        foreach ($products as $product) {
            $output[] = $this->orderProductConverter->convert($product);
        }

        return $output;
    }

    private function getOrderNumber(Order $order): string
    {
        return date('Y') . '-' . str_pad(
            (string)$order->getId(),
            5,
            '0',
            STR_PAD_LEFT
        );
    }
}
