<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Invoices\Services;

use Bittacora\Bpanel4\Invoices\Dtos\TaxBreakdown;
use Bittacora\Bpanel4\Orders\Models\Order\OrderProduct;
use Bittacora\Bpanel4\Prices\Types\Price;
use Bittacora\Bpanel4\Vat\Services\ShippingVatService;
use Bittacora\Bpanel4\Vat\Services\VatService;
use Illuminate\Database\Eloquent\Collection;

final class TaxBreakdownCalculator
{
    /** @var TaxBreakdown[] */
    private array $taxRows;

    public function __construct(
        private readonly ShippingVatService $shippingVatService,
        private readonly VatService $vatService,
    ) {
    }

    /**
     * @param Collection<int, OrderProduct>|array<OrderProduct> $products
     * @return TaxBreakdown[]
     */
    public function calculateBreakdown(Collection|array $products, ?Price $shippingCosts, array $discountByUnit = []): array
    {
        foreach ($products as $product) {
            $discountByUnitForProduct = $discountByUnit[$product->getProductId()] ?? 0;
            $taxBreakdown = $this->getTaxRow($product->getTaxRate());
            $taxBreakdown->addToTotalWithoutTaxes(
                ($product->getPaidUnitPrice()->toFloat() - $discountByUnitForProduct) * $product->getQuantity()
            );
            $taxBreakdown->addToTotalWithTaxes(
                ($product->getPaidUnitPrice()->toFloat() - $discountByUnitForProduct + $product->getTaxAmount()->toFloat()) *
                $product->getQuantity()
            );
        }

        $this->addShippingCosts($shippingCosts);

        return $this->taxRows;
    }

    private function getTaxRow(float $taxRate): TaxBreakdown
    {
        $taxRate = (string) $taxRate;

        if (!isset($this->taxRows[$taxRate])) {
            $this->taxRows[$taxRate] = new TaxBreakdown($taxRate);
        }

        return $this->taxRows[$taxRate];
    }

    private function addShippingCosts(Price $shippingCosts): void
    {
        $shippingVat = $this->shippingVatService->getVatRateForShipping();
        $taxBreakdown = $this->getTaxRow($shippingVat->getRate());

        $taxBreakdown->addToTotalWithoutTaxes($shippingCosts->toFloat());
        $shippingWithVat = Price::fromInt($this->vatService->addVatToPrice($shippingCosts->toInt(), $shippingVat));
        $taxBreakdown->addToTotalWithTaxes($shippingWithVat->toFloat());
    }
}
