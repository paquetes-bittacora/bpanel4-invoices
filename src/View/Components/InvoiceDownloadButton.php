<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Invoices\View\Components;

use Bittacora\Bpanel4\Invoices\Models\Invoice;
use Bittacora\Bpanel4\Orders\Models\Order\Order;
use Illuminate\Support\Facades\URL;
use Illuminate\View\Component;

final class InvoiceDownloadButton extends Component
{
    public bool $isAvailable = false;
    public string $url = '';

    public function __construct(protected readonly Order $order) {
        $invoice = Invoice::where('order_id', $this->order->id)->first();

        if (null !== $invoice) {
            $this->isAvailable = true;
            $this->url = URL::temporarySignedRoute(
                'bpanel4-public-invoices.download',
                now()->addMinutes(10),
                ['invoice' => $invoice->id]
            );
        }
    }

    public function render()
    {
        return view('bpanel4-invoices::components.invoice-download-button');
    }
}