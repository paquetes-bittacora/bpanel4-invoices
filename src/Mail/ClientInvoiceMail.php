<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Invoices\Mail;

use Bittacora\Bpanel4\Orders\Models\Order\Order;
use Illuminate\Mail\Attachment;
use Illuminate\Mail\Mailable;

final class ClientInvoiceMail extends Mailable
{
    public function __construct(private readonly Order $order, private readonly string $file)
    {
    }

    public function build(): ClientInvoiceMail
    {
        $client = $this->order->getClient();
        return $this->subject('Se ha generado la factura para el pedido')
            ->to($client->getEmail())
            ->view('bpanel4-invoices::mail.client-invoice', [
                'clientDetails' => $client,
                'client' => $client,
                'order' => $this->order,
            ]);
    }

    /** @return array<Attachment> */
    public function attachments(): array
    {
        $file = $this->file;
        $file = str_replace(storage_path('app') . DIRECTORY_SEPARATOR, '', $file);
        return [Attachment::fromStorage($file)];
    }
}