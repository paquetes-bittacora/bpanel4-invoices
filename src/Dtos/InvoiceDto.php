<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Invoices\Dtos;

final class InvoiceDto
{
    public function __construct(
        public readonly string $invoiceNumber,
        public readonly string $invoiceDate,
        public readonly string $shopName,
        public readonly string $shopCif,
        public readonly string $shopAddress,
        public readonly string $clientName,
        public readonly string $clientNif,
        public readonly string $shippingName,
        public readonly string $shippingSurname,
        public readonly string $shippingAddress,
        public readonly string $shippingLocation,
        public readonly string $shippingPostalCode,
        public readonly string $shippingCountry,
        public readonly string $shippingState,
        public readonly string $billingName,
        public readonly string $billingSurname,
        public readonly string $billingAddress,
        public readonly string $billingLocation,
        public readonly string $billingPostalCode,
        public readonly string $billingCountry,
        public readonly string $billingState,
        public readonly string $orderDate,
        public readonly string $orderNumber,
        /** @var InvoiceProductDto[] */
        public readonly array $products,
        public readonly float $subtotalWithoutTaxes,
        public readonly float $shippingCosts,
        public readonly float $shippingCostsTaxes,
        public readonly float $orderTotal,
        public readonly string $logoPath,
        /**
         * @var TaxBreakdown[]
         */
        public readonly array $taxBreakdown,
        public readonly ?string $vatNumber = null,
        public readonly ?string $paymentMethod = null,
        public readonly ?string $clientPhone = null,
        public readonly ?string $clientEmail = null,
        /** @var array<string, string|int|float> */
        public readonly ?array $coupons = [],
        public readonly ?string $orderNotes = null,
        public readonly ?string $shippingPhone = null,
        public readonly ?string $billingPhone = null,
        public readonly array $giftedProducts = [],
        public readonly ?string $shippingNif = null,
        public readonly ?string $billingNif = null,
    ) {
    }

    public function billingAddressIsDifferentFromShippingAddress(): bool
    {
        return
            $this->billingAddress !== $this->shippingAddress ||
            $this->billingName !== $this->shippingName ||
            $this->billingSurname !== $this->shippingSurname
        ;
    }
}
