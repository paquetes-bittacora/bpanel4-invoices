<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Invoices\Dtos;

final class TaxBreakdown
{
    private float $totalWithoutTaxes = 0.0;
    private float $totalWithTaxes = 0.0;

    public function __construct(private readonly string $name)
    {
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function addToTotalWithoutTaxes(float $amount): void
    {
        $this->totalWithoutTaxes += $amount;
    }

    public function addToTotalWithTaxes(float $amount): void
    {
        $this->totalWithTaxes += $amount;
    }

    public function getTotalWithoutTaxes(): float
    {
        return $this->totalWithoutTaxes;
    }

    public function getTotalWithTaxes(): float
    {
        return $this->totalWithTaxes;
    }

    public function getAppliedTaxes(): float
    {
        return $this->totalWithTaxes - $this->totalWithoutTaxes;
    }
}
