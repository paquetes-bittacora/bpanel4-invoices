<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Invoices\Dtos;

final class InvoiceProductDto
{
    public function __construct(
        public readonly string $name,
        public readonly string $quantity,
        public readonly float $unitPrice,
        public readonly string $taxRate,
        public readonly float $unitPriceWithTaxes,
        public readonly float $total, // Sin impuestos
        public readonly string $reference = '',
        public readonly array $meta = [],
    ) {
    }
}
