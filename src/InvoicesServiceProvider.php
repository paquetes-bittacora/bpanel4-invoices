<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Invoices;

use App\Http\Kernel;
use Bittacora\Bpanel4\HooksComponent\Contracts\HooksModule;
use Bittacora\Bpanel4\Invoices\Actions\DeleteInvoice;
use Bittacora\Bpanel4\Invoices\Commands\InstallCommand;
use Bittacora\Bpanel4\Invoices\Contracts\InvoiceDeleter;
use Bittacora\Bpanel4\Invoices\Contracts\InvoicesModule;
use Bittacora\Bpanel4\Invoices\Events\InvoiceHasBeenGenerated;
use Bittacora\Bpanel4\Invoices\Hooks\ShopConfigBeforeInvoiceFieldsHook;
use Bittacora\Bpanel4\Invoices\Listeners\InvoiceHasBeenGeneratedListener;
use Bittacora\Bpanel4\Invoices\Listeners\ShopConfigurationUpdatedListener;
use Bittacora\Bpanel4\Invoices\Services\InvoiceModule;
use Bittacora\Bpanel4\Invoices\View\Components\InvoiceDownloadButton;
use Bittacora\Bpanel4\ShopConfiguration\Contracts\Events\ShopConfigurationUpdated;
use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;

final class InvoicesServiceProvider extends ServiceProvider
{
    private const PACKAGE_PREFIX = 'bpanel4-invoices';

    public function boot(Kernel $kernel): void
    {
        $this->loadRoutesFrom(__DIR__ . '/../routes/web.php');
        $this->loadViewsFrom(__DIR__ . '/../resources/views', self::PACKAGE_PREFIX);
        $this->commands([InstallCommand::class]);
        $this->loadMigrationsFrom(__DIR__ . '/../database/migrations');
        $this->loadTranslationsFrom(__DIR__ . '/../resources/lang/', self::PACKAGE_PREFIX);
        $this->publishes(
            [__DIR__.'/../config/bpanel4-invoices.php' => $kernel->getApplication()->configPath('bpanel4-invoices.php')],
            'invoices-config'
        );
        $this->registerNexInvoiceNumberHookAndEvent();
        $this->registerBladeComponents();
        $this->bindInterfaces();
    }

    private function registerNexInvoiceNumberHookAndEvent(): void
    {
        /** @var HooksModule $hooks */
        $hooks = $this->app->make(HooksModule::class);
        $hooks->register(
            'shop-config-before-invoice-fields',
            $this->app->make(ShopConfigBeforeInvoiceFieldsHook::class)->handle(...)
        );

        $dispatcher = $this->app->make(Dispatcher::class);
        $dispatcher->listen(ShopConfigurationUpdated::class, ShopConfigurationUpdatedListener::class);
        $dispatcher->listen(InvoiceHasBeenGenerated::class, InvoiceHasBeenGeneratedListener::class);
    }

    private function registerBladeComponents(): void
    {
        Blade::component('invoice-download-button', InvoiceDownloadButton::class);
    }

    private function bindInterfaces(): void
    {
        $this->app->bind(InvoiceDeleter::class, DeleteInvoice::class);
        $this->app->bind(InvoicesModule::class, InvoiceModule::class);
    }
}
