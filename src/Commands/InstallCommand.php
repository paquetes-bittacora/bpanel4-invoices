<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Invoices\Commands;

use Illuminate\Console\Command;

final class InstallCommand extends Command
{
    /**
     * @var string
     */
    protected $signature = 'bpanel4-invoices:install';

    /**
     * @var string
     */
    protected $description = 'Instala el paquete para generar facturas en PDF';

    public function handle(): void
    {
        $this->comment('Publicando configuración del módulo de facturas');
        $this->call('vendor:publish', ['--tag' => 'invoices-config']);
    }
}
