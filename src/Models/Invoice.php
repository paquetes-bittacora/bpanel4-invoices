<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Invoices\Models;

use Bittacora\Bpanel4\ShopConfiguration\Models\ShopConfiguration;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int $order_id
 * @property int $invoice_number
 * @property string $prefixed_invoice_number
 * @property string $invoice_path
 * @property Carbon $created_at
 * @method static self where($field, $value)
 * @method self firstOrFail()
 */
final class Invoice extends Model
{
    use SoftDeletes;

    /** @var string[]  */
    public $fillable = ['order_id'];

    public static function boot(): void
    {
        parent::boot();

        self::creating(static function (self $model): void {
            // Establecemos el número de factura actual
            $shopConfiguration = ShopConfiguration::whereId(1)->firstOrFail();
            $model->invoice_number = $shopConfiguration->next_invoice_number;
            $model->prefixed_invoice_number = $shopConfiguration->getInvoicePrefix() . $model->invoice_number .
                $shopConfiguration->getInvoiceSuffix();
        });

        self::created(static function (self $model): void {
            // Actualizamos el número de la siguiente factura
            $shopConfiguration = ShopConfiguration::whereId(1)->firstOrFail();
            $shopConfiguration->next_invoice_number += 1;
            $shopConfiguration->save();
        });
    }

    public function setOrderId(int $id): void
    {
        $this->order_id = $id;
    }

    public static function getHighestInvoiceNumber(): int
    {
        /** @phpstan-ignore-next-line  */
        return (int) (self::query()->max('invoice_number') ?? 0);
    }

    public function getInvoiceNumber(): int
    {
        return $this->invoice_number;
    }

    public function getPrefixedInvoiceNumber(): string
    {
        return $this->prefixed_invoice_number;
    }

    public function getInvoiceDate(): Carbon
    {
        return $this->created_at;
    }

    public function setInvoicePath(string $path): void
    {
        $this->invoice_path = $path;
    }

    public function getInvoicePath(): string
    {
        return $this->invoice_path;
    }
}
