<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Invoices\Listeners;

use Bittacora\Bpanel4\Invoices\Exceptions\InvoiceNumberIsTakenException;
use Bittacora\Bpanel4\Invoices\Models\Invoice;
use Bittacora\Bpanel4\ShopConfiguration\Contracts\Events\ShopConfigurationUpdated;
use Bittacora\Bpanel4\ShopConfiguration\Models\ShopConfiguration;
use InvalidArgumentException;

final class ShopConfigurationUpdatedListener
{
    /**
     * @throws InvoiceNumberIsTakenException
     */
    public function handle(ShopConfigurationUpdated $event): void
    {
        $request = $event->request;

        if($request->has('next_invoice_number')) {
            $nextInvoiceNumber = $request->request->get('next_invoice_number');
            $this->validateNextInvoiceNumber($nextInvoiceNumber);
            $shopConfiguration = $this->getShopConfiguration();
            $shopConfiguration->next_invoice_number = $nextInvoiceNumber;
            $shopConfiguration->save();
        }
    }

    /**
     * @throws InvoiceNumberIsTakenException
     */
    private function validateNextInvoiceNumber(float|bool|int|string|null $nextInvoiceNumber): void
    {
        if(!is_numeric($nextInvoiceNumber) || $nextInvoiceNumber <= 0) {
            throw new InvalidArgumentException("'$nextInvoiceNumber' no es un número de factura válido.");
        }

        if (Invoice::where('invoice_number', $nextInvoiceNumber)->exists()) {
            throw new InvoiceNumberIsTakenException();
        }
    }

    private function getShopConfiguration(): ShopConfiguration
    {
        return ShopConfiguration::whereId(1)->firstOrFail();
    }

}