<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Invoices\Listeners;

use Bittacora\Bpanel4\Invoices\Events\InvoiceHasBeenGenerated;
use Bittacora\Bpanel4\Invoices\Mail\ClientInvoiceMail;
use Illuminate\Contracts\Config\Repository;
use Illuminate\Contracts\Mail\Mailer;

final class InvoiceHasBeenGeneratedListener
{
    public function __construct(
        private readonly Repository $config,
        private readonly Mailer $mailer,
    ) {
    }

    public function handle(InvoiceHasBeenGenerated $event): void
    {
        $order = $event->order;
        $file = $event->file;

        if (
            $this->config->get('bpanel4-invoices.send_invoice_to_email_when_generated', false) === true
        ) {
            $mailable = new ClientInvoiceMail($order, $file);
            $this->mailer->send($mailable);
        }
    }
}