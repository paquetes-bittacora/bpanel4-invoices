<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Invoices\Events;

use Bittacora\Bpanel4\Orders\Models\Order\Order;
use Illuminate\Foundation\Bus\Dispatchable;

final class InvoiceHasBeenGenerated
{
    use Dispatchable;

    public function __construct(
        public readonly Order $order,
        public readonly string $file
    ) {
    }
}