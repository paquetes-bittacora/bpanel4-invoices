<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Invoices\Hooks;

use Bittacora\Bpanel4\ShopConfiguration\Models\ShopConfiguration;
use Illuminate\Contracts\View\Factory;

final class ShopConfigBeforeInvoiceFieldsHook
{
    public function __construct(private readonly Factory $view)
    {
    }

    public function handle(): void
    {
        $currentNextInvoiceNumber = $this->getCurrentNextInvoiceNumber();
        echo $this->view->make('bpanel4-invoices::bpanel.invoice-number-configuration', [
            'currentNextInvoiceNumber' => $currentNextInvoiceNumber
        ])->render();
    }

    private function getCurrentNextInvoiceNumber(): int
    {
        /** @phpstan-ignore-next-line */
        return (int) ShopConfiguration::firstOrFail()->next_invoice_number;
    }
}