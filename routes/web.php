<?php

declare(strict_types=1);

use Bittacora\Bpanel4\Invoices\Http\Controllers\BpanelInvoicesController;
use Bittacora\Bpanel4\Invoices\Http\Controllers\BpanelInvoicesPublicController;
use Illuminate\Support\Facades\Route;

Route::prefix('bpanel/facturas')->name('bpanel4-invoices.')->middleware(['web', 'auth', 'admin-menu'])
    ->group(static function () {
        Route::get('{order}', [BpanelInvoicesController::class, 'getInvoice'])->name('get-invoice');
    });

Route::prefix('facturas')->name('bpanel4-public-invoices.')->middleware(['web', 'auth'])
    ->group(static function () {
        Route::get('/descargar/{invoice}', [BpanelInvoicesPublicController::class, 'downloadInvoice'])
            ->name('download');
    });
