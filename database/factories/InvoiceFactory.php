<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Invoices\Database\Factories;

use Bittacora\Bpanel4\Invoices\Models\Invoice;
use Bittacora\Bpanel4\Orders\Database\Factories\OrderFactory;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Database\Eloquent\Model;

/**
 * @extends Factory<Invoice>
 */
final class InvoiceFactory extends Factory
{
    /**
     * @var class-string<Invoice>
     */
    protected $model = Invoice::class;

    /**
     * @return array<string, Model>|array<string, string>
     */
    public function definition(): array
    {
        return [
            'order_id' => (new OrderFactory())->createOne()->getId(),
        ];
    }
}
