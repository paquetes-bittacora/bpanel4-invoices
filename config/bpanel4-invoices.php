<?php

declare(strict_types=1);

return [
    'invoice_template' => 'bpanel4-invoices::invoice',
    'send_invoice_to_email_when_generated' => true,
];
