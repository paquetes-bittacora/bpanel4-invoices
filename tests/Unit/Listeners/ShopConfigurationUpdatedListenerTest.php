<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Invoices\Tests\Unit\Listeners;

use Bittacora\Bpanel4\Invoices\Database\Factories\InvoiceFactory;
use Bittacora\Bpanel4\Invoices\Listeners\ShopConfigurationUpdatedListener;
use Bittacora\Bpanel4\ShopConfiguration\Contracts\Events\ShopConfigurationUpdated;
use Bittacora\Bpanel4\ShopConfiguration\Models\ShopConfiguration;
use Bittacora\Bpanel4\ShopConfigurations\Database\Factories\ShopConfigurationFactory;
use Bittacora\Bpanel4Users\Tests\Helpers\AdminHelper;
use Illuminate\Contracts\Routing\UrlGenerator;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Event;
use Tests\TestCase;
use function random_int;

final class ShopConfigurationUpdatedListenerTest extends TestCase
{
    use RefreshDatabase;

    private readonly UrlGenerator $urlGenerator;

    protected function setUp(): void
    {
        parent::setUp();
        $this->urlGenerator = $this->app->make(UrlGenerator::class);
        (new ShopConfigurationFactory())->createOne();
        (new AdminHelper())->actingAsAdminWithPermissions([
            'bpanel4-shop-configuration.bpanel.update',
            'bpanel4-shop-configuration.bpanel.edit',
        ]);
        $this->withoutExceptionHandling();
    }

    public function testSeRegistraElListenerParaElEventoShopConfigurationUpdated(): void
    {
        // Arrange
        Event::fake();

        // Assert
        Event::assertListening(
            ShopConfigurationUpdated::class,
            ShopConfigurationUpdatedListener::class
        );
    }

    public function testSeModificaElNumeroDeLaSiguienteFactura(): void
    {
        // Arrange
        $nextInvoiceNumber = random_int(1,99999);

        // Act
        $result = $this->put(
            $this->urlGenerator->route('bpanel4-shop-configuration.bpanel.update'),
            $this->getShopConfigurationDefaultValues() + ['next_invoice_number' => $nextInvoiceNumber]
        );

        // Assert
        $this->assertDatabaseHas(ShopConfiguration::class, [
            'next_invoice_number' => $nextInvoiceNumber,
            'id' => 1,
        ]);
    }

    public function testNoSePuedeEstablecerUnNumeroDeFacturaQueNoSeaUnNumero(): void
    {
        // Arrange

        // Act
        $result = $this->followingRedirects()->put(
            $this->urlGenerator->route('bpanel4-shop-configuration.bpanel.update'),
            $this->getShopConfigurationDefaultValues() + ['next_invoice_number' => '']
        );

        // Assert
        $result->assertSee("'' no es un número de factura válido.");
    }

    public function testNoSePuedeEstablecerUnNumeroDeFacturaQueYaEstaEnUso(): void
    {
        // Arrange
        $invoice = (new InvoiceFactory())->createOne();

        // Act
        $result = $this->followingRedirects()->put(
            $this->urlGenerator->route('bpanel4-shop-configuration.bpanel.update'),
            $this->getShopConfigurationDefaultValues() + ['next_invoice_number' => $invoice->getInvoiceNumber()]
        );

        // Assert
        $result->assertSee(e('El número de factura introducido ya está en uso'));
    }

    /**
     * @return array<string,string>
     */
    private function getShopConfigurationDefaultValues(): array
    {
        return [
            'name' => 'Nombre de prueba',
            'address' => 'Dirección de prueba',
            'cif' => '00000000A',
            'invoicePrefix' => '',
            'invoiceSuffix' => '',
            'invoiceLogo' => '',
        ];
    }
}