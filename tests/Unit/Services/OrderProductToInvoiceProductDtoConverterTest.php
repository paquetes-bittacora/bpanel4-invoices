<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Invoices\Tests\Unit\Services;

use Bittacora\Bpanel4\Invoices\Services\OrderProductToInvoiceProductDtoConverter;
use Bittacora\Bpanel4\Orders\Database\Factories\OrderFactory;
use Bittacora\Bpanel4\Orders\Models\Order\Order;
use Bittacora\Bpanel4\Orders\Models\Order\OrderProduct;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

final class OrderProductToInvoiceProductDtoConverterTest extends TestCase
{
    use RefreshDatabase;

    private OrderProductToInvoiceProductDtoConverter $converter;
    private Order $order;

    protected function setUp(): void
    {
        parent::setUp();
        $this->order = (new OrderFactory())->getFullOrder();

        /** @phpstan-ignore-next-line  */
        $this->converter = $this->app->make(OrderProductToInvoiceProductDtoConverter::class);
    }

    public function testConvierteUnOrderProductAInvoiceProductDto(): void
    {
        /** @var OrderProduct[] $products */
        $products = $this->order->getProducts();
        $productUnitPriceWithTaxes = $products[0]->getPaidUnitPrice()->toFloat() + $products[0]->getTaxAmount()->toFloat();

        $dto = $this->converter->convert($products[0]);

        self::assertEquals($products[0]->getName(), $dto->name);
        self::assertEquals($products[0]->getQuantity(), $dto->quantity);
        self::assertEquals($products[0]->getPaidUnitPrice()->toFloat(), $dto->unitPrice); // precio ud. sin IVA
        self::assertEquals($products[0]->getTaxRate(), $dto->taxRate);
        self::assertEquals($productUnitPriceWithTaxes, $dto->unitPriceWithTaxes);
        self::assertEquals($products[0]->getTotal()->toFloat(), $dto->total); // precio total con IVA
    }
}
