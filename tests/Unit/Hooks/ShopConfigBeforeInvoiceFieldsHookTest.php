<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Invoices\Tests\Unit\Hooks;

use Bittacora\Bpanel4\ShopConfiguration\Models\ShopConfiguration;
use Bittacora\Bpanel4Users\Tests\Helpers\AdminHelper;
use Illuminate\Contracts\Routing\UrlGenerator;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Random\RandomException;
use Spatie\Permission\Models\Role;
use Tests\TestCase;
use function random_int;

final class ShopConfigBeforeInvoiceFieldsHookTest extends TestCase
{
    use RefreshDatabase;

    private UrlGenerator $urlGenerator;

    protected function setUp(): void
    {
        parent::setUp();
        $this->withoutExceptionHandling();
        $this->urlGenerator = $this->app->make(UrlGenerator::class);
        Role::create(['name' => 'any-user']);
        Role::create(['name' => 'registered-user']);
        (new AdminHelper())->actingAsAdminWithPermissions(['bpanel4-shop-configuration.bpanel.edit']);
        $this->createShopConfiguration();
    }

    /**
     * @throws RandomException
     */
    public function testElCampoReflejaElValorEstablecidoParaLaSiguienteFacturaEnElApartadoDeConfiguracionDeLaTienda(): void
    {
        // Arrange
        $nextInvoiceNumber = random_int(1, 99999);
        $this->setNextInvoiceNumber($nextInvoiceNumber);

        // Act
        $result = $this->followingRedirects()
            ->get($this->urlGenerator->route('bpanel4-shop-configuration.bpanel.edit'));

        // Assert
        $result->assertSee('Nº de la siguiente factura');
        $result->assertSee($nextInvoiceNumber);
    }

    private function createShopConfiguration(): void
    {
        $shopConfiguration = new ShopConfiguration();
        $shopConfiguration->setInvoicePrefix('prefijo');
        $shopConfiguration->setInvoiceSuffix('sufijo');
        $shopConfiguration->save();
    }

    private function setNextInvoiceNumber(int $invoiceNumber): void
    {
        $shopConfiguration = ShopConfiguration::whereId(1)->firstOrFail();
        $shopConfiguration->next_invoice_number = $invoiceNumber;
        $shopConfiguration->save();
    }
}