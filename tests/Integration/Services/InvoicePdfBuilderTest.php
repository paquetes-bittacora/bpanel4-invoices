<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Invoices\Tests\Integration\Services;

use Bittacora\Bpanel4\Invoices\Dtos\InvoiceDto;
use Bittacora\Bpanel4\Invoices\Exceptions\CouldNotCreateInvoiceException;
use Bittacora\Bpanel4\Invoices\Models\Invoice;
use Bittacora\Bpanel4\Invoices\Services\InvoicePdfBuilder;
use Bittacora\Bpanel4\Orders\Database\Factories\OrderFactory;
use Bittacora\Bpanel4\ShopConfiguration\Models\ShopConfiguration;
use Illuminate\Filesystem\FilesystemManager;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

final class InvoicePdfBuilderTest extends TestCase
{
    use RefreshDatabase;

    private FilesystemManager $storage;

    public function setUp(): void
    {
        parent::setUp();
        $this->storage = $this->app->make(FilesystemManager::class);
    }

    /**
     * @throws CouldNotCreateInvoiceException
     */
    public function testGeneraElPdf(): void
    {
        $filePath = $this->generatePdf();
        self::assertFileExists($this->storage->disk('public')->path($filePath));
        $this->storage->disk('public')->delete($filePath);
    }

    public function testGuardaLaRutaAlPdfEnBd(): void
    {
        $filePath = $this->generatePdf();
        $invoice = Invoice::where('invoice_path', $filePath)->firstOrFail();
        /** @phpstan-ignore-next-line PHPStan sabe que esto siempre será true, pero lo dejo de todas formas */
        self::assertInstanceOf(Invoice::class, $invoice);
    }

    private function getInvoiceDto(): InvoiceDto
    {
        $invoice = $this->prepareOrderAndInvoice();
        return new InvoiceDto(
            invoiceNumber: $invoice->getPrefixedInvoiceNumber(),
            invoiceDate: '',
            shopName: '',
            shopCif: '',
            shopAddress: '',
            clientName: '',
            clientNif: '',
            shippingName: 'Nombre',
            shippingSurname: 'Apellidos',
            shippingAddress: '',
            shippingLocation: '',
            shippingPostalCode: '',
            shippingCountry: '',
            shippingState: '',
            billingName: 'Nombre',
            billingSurname: 'Apellidos',
            billingAddress: '',
            billingLocation: '',
            billingPostalCode: '',
            billingCountry: '',
            billingState: '',
            orderDate: '',
            orderNumber: '',
            products: [],
            subtotalWithoutTaxes: 0.0,
            shippingCosts: 0.0,
            shippingCostsTaxes: 0,
            orderTotal: 0.0,
            logoPath: '',
            taxBreakdown: []
        );
    }

    private function prepareOrderAndInvoice(): Invoice
    {
        ShopConfiguration::create();
        $order = (new OrderFactory())->createOne();
        $invoice = new Invoice();
        $invoice->setOrderId($order->getId());
        $invoice->save();
        return $invoice;
    }

    private function generatePdf(): string
    {
        $invoicePdfBuilder = $this->app->make(InvoicePdfBuilder::class);
        return $invoicePdfBuilder->generate($this->getInvoiceDto());
    }
}
