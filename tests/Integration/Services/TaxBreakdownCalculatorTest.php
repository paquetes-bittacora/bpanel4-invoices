<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Invoices\Tests\Integration\Services;

use Bittacora\Bpanel4\Invoices\Services\TaxBreakdownCalculator;
use Bittacora\Bpanel4\Orders\Database\Factories\OrderFactory;
use Bittacora\Bpanel4\Orders\Database\Factories\OrderProductFactory;
use Bittacora\Bpanel4\Orders\Models\Order\OrderProduct;
use Bittacora\Bpanel4\Prices\Exceptions\InvalidPriceException;
use Bittacora\Bpanel4\Prices\Types\Price;
use Bittacora\Bpanel4\Vat\Database\Factories\VatRateFactory;
use Bittacora\Bpanel4\Vat\Models\VatRate;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

final class TaxBreakdownCalculatorTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @throws InvalidPriceException
     */
    public function testHaceElDesglosePorTipoDeIva(): void
    {
        $taxBreakdownCalculator = $this->app->make(TaxBreakdownCalculator::class);
        $this->createShippingVat();

        $result = $taxBreakdownCalculator->calculateBreakdown([
            $this->createOrderProduct(10, 2.1, 21),
            $this->createOrderProduct(20, 4.2, 21),
            $this->createOrderProduct(100, 10, 10),
        ], new Price(0));

        self::assertEquals(30, $result[21]->getTotalWithoutTaxes());
        self::assertEquals(36.3, $result[21]->getTotalWithTaxes());
        self::assertEquals(100, $result[10]->getTotalWithoutTaxes());
        self::assertEquals(110, $result[10]->getTotalWithTaxes());
    }

    private function createOrderProduct(float $unitPrice, float $taxAmount, float $taxRate): OrderProduct
    {
        return (new OrderProductFactory())->for((new OrderFactory()), 'order')->withUnitPrice($unitPrice)
            ->withTaxRate($taxRate)->withTaxAmount($taxAmount)->createOne();
    }

    private function createShippingVat()
    {
        if (VatRate::where('apply_to_shipping', 1)->count() === 0) {
            $vatRate = (new VatRateFactory())->createOne();
            $vatRate->setApplyToShipping(true);
            $vatRate->save();
        }
    }
}
