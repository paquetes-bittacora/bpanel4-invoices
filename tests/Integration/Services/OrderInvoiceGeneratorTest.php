<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Invoices\Tests\Integration\Services;

use Bittacora\Bpanel4\Invoices\Exceptions\CouldNotCreateInvoiceException;
use Bittacora\Bpanel4\Invoices\Services\OrderInvoiceGenerator;
use Bittacora\Bpanel4\Orders\Database\Factories\OrderFactory;
use Bittacora\Bpanel4\ShopConfiguration\Models\ShopConfiguration;
use Bittacora\Bpanel4\Vat\Database\Factories\VatRateFactory;
use Bittacora\Bpanel4\Vat\Models\VatRate;
use Doctrine\DBAL\Driver\Exception;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Mail;
use Tests\TestCase;
use Throwable;

final class OrderInvoiceGeneratorTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @throws CouldNotCreateInvoiceException
     * @throws Throwable
     * @throws Exception
     */
    public function testGeneraElPdf(): void
    {
        $this->getConfiguration();
        $this->createShippingVat();
        Mail::fake();

        /** @var OrderInvoiceGenerator $generator */
        $generator = $this->app->make(OrderInvoiceGenerator::class);
        ob_start();
        $this->assertNull($generator->downloadInvoice((new OrderFactory())->getFullOrder()));
        ob_end_clean();
    }

    private function getConfiguration(): void
    {
        $shopConfiguration = ShopConfiguration::create();
        $shopConfiguration->setName('Prueba');
        $shopConfiguration->setCif('12341234A');
        $shopConfiguration->setAddress('Dirección');
        $shopConfiguration->save();
    }

    private function createShippingVat()
    {
        if (VatRate::where('apply_to_shipping', 1)->count() === 0) {
            $vatRate = (new VatRateFactory())->createOne();
            $vatRate->setApplyToShipping(true);
            $vatRate->save();
        }
    }
}
