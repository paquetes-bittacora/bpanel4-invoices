<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Invoices\Tests\Integration\Services;

use Bittacora\Bpanel4\Invoices\Dtos\InvoiceDto;
use Bittacora\Bpanel4\Invoices\Dtos\InvoiceProductDto;
use Bittacora\Bpanel4\Invoices\Dtos\TaxBreakdown;
use Bittacora\Bpanel4\Invoices\Services\InvoiceHtmlRenderer;
use Bittacora\LaravelTestingHelpers\Views\VendorViewsDisabler;
use Tests\TestCase;

final class InvoiceHtmlRendererTest extends TestCase
{
    private InvoiceHtmlRenderer $invoiceHtmlRenderer;

    protected function setUp(): void
    {
        parent::setUp();
        $this->invoiceHtmlRenderer = $this->app->make(InvoiceHtmlRenderer::class);
        VendorViewsDisabler::disableViews();
    }

    /**
     * La palabra "factura"  debe estar presente obligatoriamente por ley.
     */
    public function testIncluyeLaPalabraFactura(): void
    {
        $this->assertInvoiceContains('FACTURA');
    }

    public function testIncluyeElNumeroDeFactura(): void
    {
        $this->assertInvoiceContains($this->getInvoiceDto()->invoiceNumber);
    }

    public function testIncluyeLosDatosDelCliente(): void
    {
        $this->assertInvoiceContains($this->getInvoiceDto()->shippingName);
        $this->assertInvoiceContains($this->getInvoiceDto()->shippingSurname);
    }

    public function testIncluyeLosDatosDeLaTienda(): void
    {
        $this->assertInvoiceContains($this->getInvoiceDto()->shopName);
        $this->assertInvoiceContains($this->getInvoiceDto()->shopCif);
        $this->assertInvoiceContains($this->getInvoiceDto()->shopAddress);
    }

    public function testIncluyeLaFechaDeLaFactura(): void
    {
        $this->assertInvoiceContains($this->getInvoiceDto()->invoiceDate);
    }

    public function testIncluyeLaDireccionDeEnvio(): void
    {
        $this->assertInvoiceContains($this->getInvoiceDto()->shippingAddress);
        $this->assertInvoiceContains($this->getInvoiceDto()->shippingLocation);
        $this->assertInvoiceContains($this->getInvoiceDto()->shippingPostalCode);
        $this->assertInvoiceContains($this->getInvoiceDto()->shippingCountry);
        $this->assertInvoiceContains($this->getInvoiceDto()->shippingState);
    }

    public function testIncluyeLaDireccionDeFacturacion(): void
    {
        $this->assertInvoiceContains($this->getInvoiceDto()->billingAddress);
        $this->assertInvoiceContains($this->getInvoiceDto()->billingLocation);
        $this->assertInvoiceContains($this->getInvoiceDto()->billingPostalCode);
        $this->assertInvoiceContains($this->getInvoiceDto()->billingCountry);
        $this->assertInvoiceContains($this->getInvoiceDto()->billingState);
    }

    public function testIncluyeLaFechaDelPedido(): void
    {
        $this->assertInvoiceContains($this->getInvoiceDto()->orderDate);
    }

    public function testIncluyeElNumeroDePedido(): void
    {
        $this->assertInvoiceContains($this->getInvoiceDto()->orderNumber);
    }

    public function testIncluyeLosProductos(): void
    {
        $this->assertInvoiceContains($this->getInvoiceDto()->products[0]->name);
        $this->assertInvoiceContains($this->getInvoiceDto()->products[0]->taxRate);
        $this->assertInvoiceContains($this->getInvoiceDto()->products[0]->unitPrice);
        $this->assertInvoiceContains($this->getInvoiceDto()->products[0]->quantity);
        $this->assertInvoiceContains($this->getInvoiceDto()->products[0]->total);

        $this->assertInvoiceContains($this->getInvoiceDto()->products[1]->name);
        $this->assertInvoiceContains($this->getInvoiceDto()->products[1]->taxRate);
        $this->assertInvoiceContains($this->getInvoiceDto()->products[1]->unitPrice);
        $this->assertInvoiceContains($this->getInvoiceDto()->products[1]->quantity);
        $this->assertInvoiceContains($this->getInvoiceDto()->products[1]->total);
    }

    public function testIncluyeElSubtotalSinImpuestos(): void
    {
        $this->assertInvoiceContains($this->getInvoiceDto()->subtotalWithoutTaxes);
    }

    public function testIncluyeLosGastosDeEnvio(): void
    {
        $this->assertInvoiceContains($this->getInvoiceDto()->shippingCosts);
    }

    public function testIncluyeElTotalDelPedido(): void
    {
        $this->assertInvoiceContains($this->getInvoiceDto()->orderTotal);
    }

    public function testIncluyeElDesgloseDeIva(): void
    {
        $this->assertInvoiceContains($this->getInvoiceDto()->taxBreakdown[0]->getName());
        $this->assertInvoiceContains($this->getInvoiceDto()->taxBreakdown[0]->getTotalWithoutTaxes());
        $this->assertInvoiceContains($this->getInvoiceDto()->taxBreakdown[0]->getAppliedTaxes());

        $this->assertInvoiceContains($this->getInvoiceDto()->taxBreakdown[1]->getName());
        $this->assertInvoiceContains($this->getInvoiceDto()->taxBreakdown[1]->getTotalWithoutTaxes());
        $this->assertInvoiceContains($this->getInvoiceDto()->taxBreakdown[1]->getAppliedTaxes());
    }

    public function testIncluyeElNifDeLaDireccionDeFacturacion(): void
    {
        // Arrange

        // Act
        $this->assertInvoiceContains('98765432W');

        // Assert
    }

    private function assertInvoiceContains(float|string $value): void
    {
        if (is_float($value)) {
            $value = $this->formatPriceToString($value);
        }
        self::assertStringContainsString($value, $this->invoiceHtmlRenderer->render($this->getInvoiceDto()));
    }

    private function getInvoiceDto(): InvoiceDto
    {
        return new InvoiceDto(
            invoiceNumber: '322',
            invoiceDate: '1/1/2022',
            shopName: 'Nombre de la tienda',
            shopCif: 'Z111111111',
            shopAddress: 'Dirección de la tienda',
            clientName: 'Nombre del cliente',
            clientNif: '80660807Z',
            shippingName: 'Nombre',
            shippingSurname: 'Apellidos',
            shippingAddress: 'Dirección de envío',
            shippingLocation: 'Ciudad de envío',
            shippingPostalCode: '99999',
            shippingCountry: 'País de envío',
            shippingState: 'Provincia de envío',
            billingName: 'Nombre',
            billingSurname: 'Apellidos',
            billingAddress: 'Dirección de facturación',
            billingLocation: 'Ciudad de facturación',
            billingPostalCode: '88888',
            billingCountry: 'País de facturación',
            billingState: 'Provincia de facturación',
            orderDate: '2/2/2222',
            orderNumber: '951',
            products: [
                new InvoiceProductDto(
                    'Producto 1',
                    '55',
                    54,
                    '21',
                    65.34,
                    3528.36
                ),
                new InvoiceProductDto(
                    'Producto 2',
                    '33',
                    32,
                    '21',
                    38.72,
                    1222.76
                ),
            ],
            subtotalWithoutTaxes: 121.212,
            shippingCosts: 321.321,
            shippingCostsTaxes: 0,
            orderTotal: 1777.777,
            logoPath: '',
            taxBreakdown: [
                $this->createTaxBreakdown('21', 101, 122.21),
                $this->createTaxBreakdown('10', 200, 220),
            ],
            shippingNif: null,
            billingNif: '98765432W'
        );
    }

    private function createTaxBreakdown(string $name, float $totalWithoutTaxes, float $totalWithTaxes): TaxBreakdown
    {
        $taxBreakdown = new TaxBreakdown($name);
        $taxBreakdown->addToTotalWithoutTaxes($totalWithoutTaxes);
        $taxBreakdown->addToTotalWithTaxes($totalWithTaxes);
        return $taxBreakdown;
    }

    private function formatPriceToString(float $price): string
    {
        return number_format($price, 2, ',', '.');
    }
}
