<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Invoices\Tests\Integration;

use Bittacora\Bpanel4\Invoices\Dtos\InvoiceDto;
use Bittacora\Bpanel4\Invoices\Services\InvoiceHtmlRenderer;
use Bittacora\Bpanel4\Orders\Database\Factories\OrderFactory;
use Bittacora\Bpanel4\Orders\Database\Factories\OrderGiftedProductFactory;
use Bittacora\Bpanel4\Orders\Models\Order\OrderGiftedProduct;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

final class GiftedProductsInInvoiceTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;

    public function testSeMuestranLosProductosRegaloEnLaFactura(): void
    {
        // Arrange
        $order = (new OrderFactory())->createOne();
        /** @var array<OrderGiftedProduct> $giftedProducts */
        $giftedProducts = (new OrderGiftedProductFactory())->for($order)->count(4)->create()->all();
        $dto = new InvoiceDto(
            invoiceNumber: (string) $this->faker->randomNumber(),
            invoiceDate: $this->faker->date(),
            shopName: $this->faker->company(),
            shopCif: 'B00000000',
            shopAddress: $this->faker->address(),
            clientName: $this->faker->name(),
            clientNif: '00000000A',
            shippingName: $this->faker->name(),
            shippingSurname: $this->faker->lastName(),
            shippingAddress: $this->faker->streetAddress(),
            shippingLocation: 'Prueba',
            shippingPostalCode: '1234',
            shippingCountry: 'España',
            shippingState: 'Prueba',
            billingName: $this->faker->name(),
            billingSurname: $this->faker->lastName(),
            billingAddress: $this->faker->streetAddress(),
            billingLocation: 'Prueba',
            billingPostalCode: '1234',
            billingCountry: 'España',
            billingState: 'Prueba',
            orderDate: $this->faker->date(),
            orderNumber: (string) $this->faker->randomNumber(),
            products: [],
            subtotalWithoutTaxes: 0,
            shippingCosts: 0,
            shippingCostsTaxes: 0,
            orderTotal: 0,
            logoPath: '',
            taxBreakdown: [],
            giftedProducts: $giftedProducts,
        );

        // Act
        $invoice = $this->app->make(InvoiceHtmlRenderer::class)->render($dto);

        // Assert
        foreach ($giftedProducts as $giftedProduct) {
            $this->assertStringContainsString($giftedProduct->getName(), $invoice);
        }
    }
}