<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Invoices\Tests\Integration\Http\Controllers;

use Bittacora\Bpanel4\Invoices\Mail\ClientInvoiceMail;
use Bittacora\Bpanel4\Orders\Database\Factories\OrderFactory;
use Bittacora\Bpanel4\ShopConfigurations\Database\Factories\ShopConfigurationFactory;
use Bittacora\Bpanel4\Vat\Database\Factories\VatRateFactory;
use Bittacora\Bpanel4\Vat\Models\VatRate;
use Bittacora\Bpanel4Users\Tests\Helpers\AdminHelper;
use Config;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Mail;
use Tests\TestCase;
use URL;

final class InvoiceControllerTest extends TestCase
{
    use RefreshDatabase;

    public function testSeGeneraElPdfDeLaFactura(): void
    {
        // Arrange
        Mail::fake();
        $order = $this->arrangeOrder();

        // Act
        $result = $this->generateInvoice($order);

        // Assert
        $result->assertOk();
    }

    public function testNoSeEnviaLaFacturaPorCorreoSiNoEstaConfigurado(): void
    {
        // Arrange
        $order = $this->arrangeOrder();
        Config::set('bpanel4-invoices.send_invoice_to_email_when_generated', false);
        $mail = Mail::fake();

        // Act
        $this->generateInvoice($order);

        // Assert
        $mail->assertNothingOutgoing();
    }

    public function testSeEnviaLaFacturaPorCorreoSiEstaConfigurado(): void
    {
        // Arrange
        $order = $this->arrangeOrder();
        Config::set('bpanel4-invoices.send_invoice_to_email_when_generated', true);
        $mail = Mail::fake();

        // Act
        $this->generateInvoice($order);

        // Assert
        $mail->assertSent(ClientInvoiceMail::class);
    }

    public function testElCorreoConLaFacturaSoloSeEnviaUnaVez(): void
    {
        // Arrange
        $order = $this->arrangeOrder();
        Config::set('bpanel4-invoices.send_invoice_to_email_when_generated', true);
        $mail = Mail::fake();

        // Act
        $this->generateInvoice($order);
        $this->generateInvoice($order);
        $this->generateInvoice($order);
        $this->generateInvoice($order);
        $this->generateInvoice($order);
        $this->generateInvoice($order);
        $this->generateInvoice($order);

        // Assert
        $mail->assertSent(ClientInvoiceMail::class);
        $mail->assertSentCount(1);
    }

    private function createShippingVat()
    {
        if (VatRate::where('apply_to_shipping', 1)->count() === 0) {
            $vatRate = (new VatRateFactory())->createOne();
            $vatRate->setApplyToShipping(true);
            $vatRate->save();
        }
    }

    private function arrangeOrder(): \Bittacora\Bpanel4\Orders\Models\Order\Order
    {
        $this->withoutExceptionHandling();
        $order = (new OrderFactory())->getFullOrder();
        (new AdminHelper())->actingAsAdminWithPermissions(['bpanel4-invoices.get-invoice']);
        (new ShopConfigurationFactory())->createOne();
        $this->createShippingVat();
        return $order;
    }

    private function generateInvoice(\Bittacora\Bpanel4\Orders\Models\Order\Order $order): \Illuminate\Testing\TestResponse
    {
        ob_start();
        $result = $this->followingRedirects()
            ->get(URL::signedRoute('bpanel4-invoices.get-invoice', ['order' => $order->getId()]));
        ob_end_clean();
        return $result;
    }

}