<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Invoices\Tests\Integration\Models;

use Bittacora\Bpanel4\Invoices\Database\Factories\InvoiceFactory;
use Bittacora\Bpanel4\Invoices\Models\Invoice;
use Bittacora\Bpanel4\ShopConfiguration\Models\ShopConfiguration;
use Bittacora\Bpanel4\ShopConfigurations\Database\Factories\ShopConfigurationFactory;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

final class InvoiceTest extends TestCase
{
    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();
        $shopConfiguration = new ShopConfiguration();
        $shopConfiguration->setInvoicePrefix('prefijo');
        $shopConfiguration->setInvoiceSuffix('sufijo');
        $shopConfiguration->save();
    }

    public function testDevuelveElNumeroDeFacturaMaximo(): void
    {
        for ($i = 0; $i < 10; ++$i) {
            self::assertEquals($i, Invoice::getHighestInvoiceNumber());
            (new InvoiceFactory())->createOne();
        }
    }

    public function testCreaLosNumerosDeFacturaConPrefijoYSufijo(): void
    {
        for ($i = 0; $i < 10; ++$i) {
            $invoice = (new InvoiceFactory())->createOne();
            self::assertEquals('prefijo' . ($i + 1) . 'sufijo', $invoice->getPrefixedInvoiceNumber());
        }
    }

    public function testSeCreanNumerosDeFacturaCorrelativos(): void
    {
        // Arrange
        (new ShopConfigurationFactory())->createOne();

        // Act
        $invoices = (new InvoiceFactory())->count(10)->create();

        // Assert
        for ($i=0; $i<=9; ++$i) {
            self::assertEquals($i + 1, $invoices[$i]->invoice_number);
        }
    }
}
