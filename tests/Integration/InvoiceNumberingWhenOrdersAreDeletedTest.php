<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Invoices\Tests\Integration;

use Bittacora\Bpanel4\Invoices\Database\Factories\InvoiceFactory;
use Bittacora\Bpanel4\Invoices\Models\Invoice;
use Bittacora\Bpanel4\Orders\Actions\Order\DeleteOrder;
use Bittacora\Bpanel4\Orders\Models\Order\Order;
use Bittacora\Bpanel4\ShopConfiguration\Models\ShopConfiguration;
use Bittacora\Bpanel4\ShopConfigurations\Database\Factories\ShopConfigurationFactory;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use function random_int;

final class InvoiceNumberingWhenOrdersAreDeletedTest extends TestCase
{
    use RefreshDatabase;

    private int $initialNextInvoiceNumber;
    private ShopConfiguration $shopConfiguration;
    private DeleteOrder $deleteOrder;

    protected function setUp(): void
    {
        parent::setUp();
        $this->initialNextInvoiceNumber = random_int(1, 9999);
        $this->shopConfiguration = (new ShopConfigurationFactory())->createOne([
            'next_invoice_number' => $this->initialNextInvoiceNumber,
        ]);
        $this->deleteOrder = $this->app->make(DeleteOrder::class);
    }

    public function testAlBorrarElUltimoPedidoSeRestaUnoAlNumeroDeLaSiguienteFactura(): void
    {
        // Arrange
        $invoices = (new InvoiceFactory())->count(4)->create();
        $this->shopConfiguration->refresh();
        $this->assertEquals($this->initialNextInvoiceNumber + 4, $this->shopConfiguration->next_invoice_number);

        // Act
        $this->deleteOrder->handle(Order::whereId($invoices[3]->order_id)->firstOrFail());

        // Assert
        $this->shopConfiguration->refresh();
        $this->assertEquals($this->initialNextInvoiceNumber + 3, $this->shopConfiguration->next_invoice_number);
    }

    public function testAlBorrarElPedidoQueNoEsElUltimoNoSeRestaUnoAlNumeroDeLaSiguienteFactura(): void
    {
        // Arrange
        $invoices = (new InvoiceFactory())->count(4)->create();
        $this->shopConfiguration->refresh();
        $this->assertEquals($this->initialNextInvoiceNumber + 4, $this->shopConfiguration->next_invoice_number);

        // Act
        $this->deleteOrder->handle(Order::whereId($invoices[2]->order_id)->firstOrFail());

        // Assert
        $this->shopConfiguration->refresh();
        $this->assertEquals($this->initialNextInvoiceNumber + 4, $this->shopConfiguration->next_invoice_number);
    }

    public function testAlHacerNuevosPedidosDespuesDeBorrarOtrosLosNumerosDeFacturaSonCorrelativos(): void
    {
        // Arrange
        $invoices = (new InvoiceFactory())->count(6)->create();

        // Act
        for($i=5; $i >= 2; --$i) {
            $this->deleteOrder->handle(Order::whereId($invoices[$i]->order_id)->firstOrFail());
        }

        (new InvoiceFactory())->count(4)->create();

        // Assert
        $allInvoices = Invoice::all();
        $expectedInvoiceNumber = $this->initialNextInvoiceNumber;
        foreach ($allInvoices as $invoice) {
            $this->assertEquals($expectedInvoiceNumber++, $invoice->invoice_number);
        }
    }
}